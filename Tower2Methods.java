// Programmer: Tianyu Ma

import java.util.Arrays;
import javax.swing.*;

public class Tower2Methods{
	//Methods() {}
	//*******************p()***************************************************
	public void p(String str)	{System.out.println(str);}
	//*******************verify()**********************************************
	public boolean verify(int INPUT)	{return (INPUT >= 3 && INPUT <= 9);}
	//*******************getPreBarX()******************************************
	public int getPreBarX(JButton currentButton){	
		int currentLayerX = currentButton.getX()+currentButton.getWidth()/2-10;
		if		(currentLayerX == 290)	return 990;
		else if	(currentLayerX == 630)	return 290;
		else 							return 630;
	}
	//*******************getCurBarX()******************************************
	public int getCurBarX(JButton currentButton){	
		int currentLayerX = currentButton.getX()+currentButton.getWidth()/2-10;
		if		(currentLayerX == 290)	return 290;
		else if	(currentLayerX == 630)	return 630;
		else 							return 990;
	}
	//*******************getNexBarX()******************************************
	public int getNexBarX(JButton currentButton){	
		int currentLayerX = currentButton.getX()+currentButton.getWidth()/2-10;
		if		(currentLayerX == 290)	return 630;
		else if	(currentLayerX == 630)	return 990;
		else 							return 290;
	}	
	//*******************getPreBarButtonNum()**********************************
	public int getPreBarButtonNum(int previousBarX, 
								  JButton buttons[], 
								  int input){
		int previousBarButtonNum = 0;
		for(int i = 0; i < input; i++){
			if((buttons[i].getX()+buttons[i].getWidth()/2-10)==previousBarX){
				previousBarButtonNum++;
			}
		}return previousBarButtonNum;
	}	
	//*******************getCurBarButtonNum()**********************************
	public int getCurBarButtonNum(int currentBarX, 
								  JButton buttons[], 
								  int INPUT){
		int currentBarButtonNum = 0;
		for(int i = 0; i < INPUT; i++){
			if((buttons[i].getX()+buttons[i].getWidth()/2-10) == currentBarX){
				currentBarButtonNum++;
			}
		}return currentBarButtonNum;
	}
	//*******************getNexBarButtonNum()**********************************
	public int getNexBarButtonNum(int nextBarX, JButton buttons[], int INPUT){
		int nextBarButtonNum = 0;
		for(int i = 0; i < INPUT; i++){	
			if(buttons[i].getX() + buttons[i].getWidth() / 2 - 10 == nextBarX){
				nextBarButtonNum++;
			}
		}return nextBarButtonNum;
	}	
	//*******************getPreBarTopButtonWidth()*****************************
	public int getPreBarTopButtonWidth(int preBarButtonNum, 
									   int preBarX, 
									   int INPUT,
									   JButton[] BUTTONS){
		int[] widthArray = new int[preBarButtonNum + 11];
		this.p("widthArray size is " + widthArray.length); 
		
		for(int i = 0; i < INPUT; i++){
			if(BUTTONS[i].getX() + BUTTONS[i].getWidth() / 2 - 10 == preBarX){
				widthArray[i] = BUTTONS[i].getWidth();
			} 
		}
		Arrays.sort(widthArray);
		return widthArray[11];
	}
	//*******************getNexBarTopButtonWidth()*****************************
	public int getNextBarTopButtonWidth(int nexBarButtonNum, 
									    int nexBarX, 
									    int INPUT,
									    JButton[] BUTTONS){
		int[] widthArray = new int[nexBarButtonNum + 11];
		this.p("widthArray size is " + widthArray.length); 
		
		for(int i = 0; i < INPUT; i++){
			if(BUTTONS[i].getX() + BUTTONS[i].getWidth() / 2 - 10 == nexBarX){
				widthArray[i] = BUTTONS[i].getWidth();
			} 
		}
		Arrays.sort(widthArray);
		return widthArray[11];
	}
	//****************************************buttonIsOnTop()******************
	public boolean buttonIsOnTop(int INPUT,
								 int currentBarX,
								 int currentBarButtonNum,
								 JButton[] BUTTONS,
								 JButton clickedButton){
		boolean result = false;
		if(currentBarButtonNum == 1)	return true;
		int oriX = clickedButton.getX() + clickedButton.getWidth() / 2 - 10;
		int oriY = clickedButton.getY();	
		for(int i = 0; i < INPUT; i++){
			int curX = BUTTONS[i].getX() + BUTTONS[i].getWidth() / 2 - 10;
			int curY = BUTTONS[i].getY();
			if(curX == currentBarX && curX == oriX && curY >  oriY){
				result = true;
			}else if(curX == currentBarX && curX == oriX && curY <  oriY){
				result = false;
				break;
			}
		}return result;
	}
	//***********************sleep()***********************
	public void sleep(long ms){
		try{Thread.sleep(ms);
		}catch(InterruptedException e){}
	}
	//*********************ifFinished()****************************************
	public boolean isItFinished(JButton[] BUTTONS, int INPUT){
		boolean result = false;
		for(int i = 0; i < INPUT; i++){
			if(BUTTONS[i].getX() + BUTTONS[i].getWidth() / 2 - 10 == 990){
				result = true;
			}else{
				return false;
			}
		}return result;
	}
}