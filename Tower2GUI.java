// Programmer: Tianyu Ma

import java.awt.*;
import javax.swing.*;

public class Tower2GUI{
	public static Tower2Methods M;	
	public static void main(String args[]){
		
//  Frame 1: welcome frame
		JFrame welcomeFrame = new JFrame("Tower 2");
		welcomeFrame.setLayout(null);
		
		//frame.setBackground(Color.gray);
		int W = 640;
		int H = 400;
		welcomeFrame.setSize(W, H);
		
		// Center the window
		Dimension dim1 = Toolkit.getDefaultToolkit().getScreenSize();
		int width1 = (dim1.width - welcomeFrame.getWidth()) / 2;
		int height1 = (dim1.height - welcomeFrame.getHeight()) / 2;
		welcomeFrame.setLocation(width1, height1);
		
		// Creating components
		JLabel l1 = new JLabel("Enter number of layers you want to play");
			l1.setFont(new Font("Serif", Font.PLAIN, 25));
		JTextField tf1 = new JTextField("3 ~ 9");
			tf1.setFont(new Font("Serif", Font.BOLD, 25));
		JLabel error = new JLabel("Please enter number between 3 and 9");
			error.setFont(new Font("Serif", Font.PLAIN, 25));
		JButton play = new JButton("Play");
			play.setFont(new Font("Serif", Font.PLAIN, 30));
		error.setForeground(Color.RED);
		
		JLabel move = new JLabel("Moves:");
			move.setFont(new Font("Serif", Font.PLAIN, 20));		
		JLabel counter = new JLabel("0");
			counter.setFont(new Font("Serif", Font.PLAIN, 20));	
		JButton repaly = new JButton("Replay!");
			repaly.setFont(new Font("Serif", Font.PLAIN, 20));
		
		// Center the text in components
		l1.setHorizontalAlignment(JLabel.CENTER);
		tf1.setHorizontalAlignment(JTextField.CENTER);
		error.setHorizontalAlignment(JLabel.CENTER);
		
		// Components' specifications
		//                 		x       y    w    h
		play.setBounds 		((W-150)/2, 270, 150, 50);
		l1.setBounds   		((W-500)/2, 100, 500, 30);
		tf1.setBounds  		((W-100)/2, 170, 100, 40);
		error.setBounds		((W-400)/2, 220, 400, 40);
		
		// Adding components to the frame
		welcomeFrame.add(play);
		welcomeFrame.add(l1);
		welcomeFrame.add(tf1);
		welcomeFrame.add(error);
		error.setVisible(false);
// end of frame 1
		
// frame 2: game frame
		JFrame gameFrame = new JFrame("Tower 2");
		gameFrame.setLayout(null);
		gameFrame.setSize(1280, 800);
		
		//frame.setBackground(Color.gray);
		Dimension dim2 = Toolkit.getDefaultToolkit().getScreenSize();
		int width2 = (dim2.width - gameFrame.getWidth()) / 2;
		int height2 = (dim2.height - gameFrame.getHeight()) / 2;
		gameFrame.setLocation(width2, height2);
		
		// Creating components
		JLabel labelA = new JLabel("A");
		JLabel labelB = new JLabel("B");
		JLabel labelC = new JLabel("C");
		labelA.setFont(new Font("Serif", Font.PLAIN, 30));
		labelB.setFont(new Font("Serif", Font.PLAIN, 30));
		labelC.setFont(new Font("Serif", Font.PLAIN, 30));
		JButton Button1 = new JButton();
		JButton Button2 = new JButton();
		JButton Button3 = new JButton();
		Button1.setEnabled(false);
		Button2.setEnabled(false);
		Button3.setEnabled(false);
		JButton base = new JButton("Try to shift all the layers " +
								   "from the stick A to C.");
		base.setEnabled(false);
		base.setFont(new Font("Serif", Font.PLAIN, 30));	
		
		// Components specifications
		//					 x    		y    w     h
		move.setBounds		((W-600)/2, 10,  100,  40);
		counter.setBounds	((W-450)/2, 10,  100,  40);
		labelA.setBounds	(290,		120, 30,   30);      // x = 290 is stick A 
		labelB.setBounds	(630, 		120, 30,   30);      // x = 630 is stick B
		labelC.setBounds	(990, 		120, 30,   30);      // x = 990 is stick C
		Button1.setBounds	(285, 		157, 30,   500);
		Button2.setBounds	(625, 		157, 30,   500);
		Button3.setBounds	(985, 		157, 30,   500);
		base.setBounds		(40, 		650, 1200, 80);	
		repaly.setBounds	(1150, 		20,	 100,  40);
		
		
		// generating up to 9 layers
		int x = 150;
		int y = 600;
		int w = 300;
		int h = 50;
		
		JButton buttons[] = new JButton[9];
		
		for(int i = 0; i < 9; i++){
			buttons[i] = new JButton("" + i);
			buttons[i].setFont(new Font("Serif", Font.PLAIN, 30));
			buttons[i].setBounds(x, y, w, h);
			x += 15;
			y -= 50;
			w -= 30;	
			
			gameFrame.add(buttons[i]);
			buttons[i].setVisible(false);
		}
		
		// Adding components to the new frame
		gameFrame.add(move);
		gameFrame.add(counter);
		gameFrame.add(base);
		gameFrame.add(labelA);
		gameFrame.add(labelB);
		gameFrame.add(labelC);
		gameFrame.add(Button1);
		gameFrame.add(Button2);
		gameFrame.add(Button3);
		gameFrame.add(repaly);
// end of frame 2

// frame 3: finishFrame
		JFrame finishFrame = new JFrame("Congratulations");
		finishFrame.setLayout(null);
// end of frame 3
				
		// frames configuration
		welcomeFrame.setVisible(true);
		welcomeFrame.setResizable(false);
		welcomeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		gameFrame.setVisible(false);
		gameFrame.setResizable(false);
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// ActionListener
		Tower2ActionListener listener = new Tower2ActionListener(welcomeFrame, 
																 gameFrame,
																 buttons,
																 play,
																 repaly,
																 counter,
																 error, 
																 tf1,
																 base);
		play.addActionListener(listener);
		repaly.addActionListener(listener);
		for(int i = 0; i < 9; i++){
			buttons[i].addActionListener(listener);
		}
	}
}