// Programmer: Tianyu Ma

import javax.swing.*;

import java.awt.Color;
import java.awt.event.*;

public class Tower2ActionListener implements ActionListener{
	private Tower2Methods M;
	private JFrame WELCOMEFRAME, GAMEFRAME;
	private JButton[] BUTTONS = new JButton[9];
	private JButton PLAY;
	private JButton REPLAY;
	private JButton BASE;
	private JTextField TF1;
	private JLabel COUNTER;
	private JLabel ERROR;
	private int clickedButtonNo = -1;
	//*******************Tower2ActionListener()********************************
	public Tower2ActionListener(JFrame welcomeFrame, 
								JFrame gameFrame,
								JButton buttons[],
								JButton play,
								JButton repaly,
								JLabel counter,
								JLabel error,
								JTextField tf1,
								JButton base){
		M 				= new Tower2Methods();
		WELCOMEFRAME 	= welcomeFrame;
		GAMEFRAME		= gameFrame;
		PLAY 			= play;
		REPLAY 			= repaly;
		BASE			= base;
		TF1 			= tf1;
		COUNTER 		= counter;
		ERROR 			= error;

		for(int i = 0; i < 9; i++)	{BUTTONS[i] = buttons[i];}
	}	
	//*******************actionPerformed()*************************************
	public void actionPerformed(ActionEvent e){
		// convert input from String to int
		int currentCount = Integer.parseInt(COUNTER.getText());
		int INPUT = Integer.parseInt(TF1.getText());
		Object event = e.getSource();
		
		if(event == PLAY){
			if((M.verify(INPUT) == false))	{ERROR.setVisible(true);}
			else{
				WELCOMEFRAME.setVisible(false);
				GAMEFRAME.setVisible(true);
				// display buttons based on the input number
				for(int i = 0; i < INPUT; i++)	{BUTTONS[i].setVisible(true);}
			}
		}
		if(event == REPLAY){
			//GAMEFRAME.setVisible(false);
			//WELCOMEFRAME.setVisible(true);
			clickedButtonNo = -1;
			currentCount = 0;
			COUNTER.setText("" + currentCount);
			
			int x = 150;
			int y = 600;
			int w = 300;
			int h = 50;
			for(int i = 0; i < 9; i++){
				BUTTONS[i].setBounds(x, y, w, h);
				x += 15;
				y -= 50;
				w -= 30;
			}
			BASE.setText("Try to shift all the layers from the stick A to C.");
		}
		// store buttons width to the array buttonsWidth[]
		int[] buttonsWidth = new int[INPUT];
		for(int i = 0; i < INPUT; i++){
			buttonsWidth[i] = BUTTONS[i].getWidth();
		}		
		// x = 290 is Bar A 
		// x = 630 is Bar B
		// x = 990 is Bar C	

		// print the clicked button number to the console
		for(int buttonNo = 0; buttonNo < 9; buttonNo++){
			if(event == BUTTONS[buttonNo])	{clickedButtonNo = buttonNo;}
		}	
		M.p("----------------------------");			
		if(clickedButtonNo > -1){
			M.p("clicked button num is " + clickedButtonNo);
			int preBarX = M.getPreBarX(BUTTONS[clickedButtonNo]);
			int curBarX = M.getCurBarX(BUTTONS[clickedButtonNo]);
			int nexBarX = M.getNexBarX(BUTTONS[clickedButtonNo]);
				
			int preBarButtonNum = M.getPreBarButtonNum(preBarX,BUTTONS,INPUT);
			int curBarButtonNum = M.getCurBarButtonNum(curBarX,BUTTONS,INPUT);
			int nexBarButtonNum = M.getNexBarButtonNum(nexBarX,BUTTONS,INPUT);
			
			boolean buttonIsOnTop = M.buttonIsOnTop(INPUT, 
													curBarX, 
													curBarButtonNum, 
													BUTTONS, 
													BUTTONS[clickedButtonNo]);
			if(buttonIsOnTop == true){	
				// next Bar does not have button(s)
				if(nexBarButtonNum == 0){
					BUTTONS[clickedButtonNo].setLocation(nexBarX - 
					BUTTONS[clickedButtonNo].getWidth()/2 + 10, 600);
					COUNTER.setText("" + (currentCount + 1));
				}
										
				// next Bar has button(s), I want to know how many button(s) 
				// on the next Bar, the the width of the top button on next Bar
				else{
					// next Bar top button's width is wider
					int nextBarTopButtonWidth = M.getNextBarTopButtonWidth(
															nexBarButtonNum, 
															nexBarX, 
															INPUT, 
															BUTTONS);
					if(nextBarTopButtonWidth > buttonsWidth[clickedButtonNo]){
						BUTTONS[clickedButtonNo].setLocation(nexBarX - 
						BUTTONS[clickedButtonNo].getWidth()/2 + 10, 600 - 
						nexBarButtonNum * 50);
						COUNTER.setText("" + (currentCount + 1));
					}
					// is previous Bar does not have button(s)?
					else{
						// previous Bar have button(s)
						if(preBarButtonNum == 0){
							int newX1 = preBarX - BUTTONS[clickedButtonNo].
										getWidth()/2 + 10;
							int newY1 = 600;
							BUTTONS[clickedButtonNo].setLocation(newX1, newY1);
							COUNTER.setText("" + (currentCount + 1));
						}
						// previous Bar top button's width is wider
						else{
							int previousBarTopButtonWidth = 
									M.getPreBarTopButtonWidth(preBarButtonNum,
															  preBarX, 
															  INPUT, 
															  BUTTONS);
							if(previousBarTopButtonWidth > 
								buttonsWidth[clickedButtonNo]){
								int X2 = preBarX - BUTTONS[clickedButtonNo].
														getWidth()/2 + 10;
								int Y2 = 600 - preBarButtonNum * 50;
								BUTTONS[clickedButtonNo].setLocation(X2, Y2);
								COUNTER.setText("" + (currentCount + 1));
							}
						}
					}	
				}
			}
		}
		// check to see if it is correctly finished
		if(M.isItFinished(BUTTONS, INPUT)){
			/*
			for(int i = 0; i < INPUT; i++){
				BUTTONS[i].setEnabled(false);
			}*/
			BASE.setText("Congratulations, you are awesome!");
		}
	}
}